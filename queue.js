let collection = [];
let tail = 0;
let head = 0;

// Write the queue functions below.

function print(){
  return collection;
};
function enqueue (data){
    if(collection.length == 0){
        collection[0] = data;
    } else {
        collection[collection.length] = data;
    }
    return collection;
};
function dequeue(){
    let tempCollection = [];
    for(let i = 0; i < collection.length - 1; i++){
        tempCollection[i] = collection[i+1];
    }
    return collection = [...new Set(tempCollection)];

  
}  
function front(){
    return collection[head]
}

function size(){
    return collection.length;
}

function isEmpty(){
    if (collection===[]){
        return true
    }
    return false
}

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};

// collection,
// print,
// enqueue,
// dequeue,
// front,
// size,
// isEmpty,
